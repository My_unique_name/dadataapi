from dadata import Dadata


class DadataApi:
    def __init__(self, token, secret, lang):
        self.__token = token
        self.__secret = secret
        self.__lang = lang

        self.__lang_mapper = {
            'en': {
                'geo_lat': 'Latitude',
                'geo_lon': 'Longitude'
            },
            'ru': {
                'geo_lat': 'Широта',
                'geo_lon': 'Долгота'
            }
        }

    def get_location_list_by_address(self, address):
        with Dadata(self.__token, self.__secret) as dadata:
            return dadata.suggest(name="address", query=address, language=self.__lang)

    def get_location_coordinates_by_address(self, dadata_address):
        with Dadata(self.__token, self.__secret) as dadata:
            response = dadata.suggest(name="address", query=dadata_address,
                                      qc_geo=0, count=1, language=self.__lang).pop()
            return {
                self.__lang_mapper[self.__lang]['geo_lat']: response['data']['geo_lat'],
                self.__lang_mapper[self.__lang]['geo_lon']: response['data']['geo_lon']
            }
