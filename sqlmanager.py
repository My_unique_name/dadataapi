import sqlite3


class DbSqliteManager:
    def __init__(self, script_path='sqlite_script', db_path='sqlite_python.db'):
        self.__db_path = db_path
        self.__script_path = script_path
        self._execute_sql_script()

    def _execute_sql_script(self):
        with sqlite3.connect(self.__db_path) as connection:
            cursor = connection.cursor()
            with open(self.__script_path, 'r') as sqlite_file:
                sql_script = sqlite_file.read()
                try:
                    cursor.executescript(sql_script)
                except sqlite3.OperationalError:
                    pass
                finally:
                    cursor.close()

    def insert_config_values(self, *args):
        with sqlite3.connect(self.__db_path) as connection:
            sql = 'insert into configurations(title, api, secret, lang) values(' + ('?,' * len(*args))[:-1] + ');'
            cur = connection.cursor()
            try:
                cur.execute(sql, *args)
            except sqlite3.Error:
                return None
            connection.commit()
            return cur.lastrowid

    def get_config_by_title(self, title):
        with sqlite3.connect(self.__db_path) as connection:
            sql = 'select * from configurations where title=(?);'
            cur = connection.cursor()
            cur.execute(sql, (title, ))
            return cur.fetchone()

    def get_config_list(self):
        with sqlite3.connect(self.__db_path) as connection:
            sql = 'select * from configurations;'
            cur = connection.cursor()
            cur.execute(sql)
            return cur.fetchall()

    def delete_config_by_title(self, title):
        with sqlite3.connect(self.__db_path) as connection:
            sql = 'delete from configurations where title=(?);'
            cur = connection.cursor()
            cur.execute(sql, (title,))
            connection.commit()

