from api import DadataApi
from sqlmanager import DbSqliteManager
from httpx import HTTPStatusError


def menu():
    api = None
    db = DbSqliteManager()
    while True:
        while True:
            config_choice = int(input('1. Использовать существующую конфигурацию\n2. Создать конфигурацию\n3. Удалить' +
                                      ' конфигурацию\n4. Список конфигураций\n5. Выход\nНомер операции:'))
            if config_choice not in range(1, 6):
                continue
            if config_choice == 1:
                user_config = db.get_config_by_title(input('Введите название конфигурации:').strip())
                if user_config is None:
                    print('Данная конфигурация отсутствует в базе данных')
                    continue
                api = DadataApi(*user_config[2:5])
                break
            elif config_choice == 2:
                new_config = input('Введите через запятую название конфигурации(максимальный размер названия 60 ' +
                                   'символов), api-ключ, секретный ключ, язык(ru или en)\nНапример:' +
                                   'test, 4bas.........,63tgdg.........,en\nДанные:')
                if db.insert_config_values([i.strip() for i in new_config.split(',')]) is None:
                    print('Ошибка при добавлении конфигурации')
            elif config_choice == 3:
                db.delete_config_by_title(input('Введите название конфигурации, которую необходимо удалить:'))
            elif config_choice == 4:
                config_list = db.get_config_list()
                if config_list:
                    for config in config_list:
                        print(config)
                else:
                    print('Список пуст')
            else:
                return
        while True:
            address = input('Введите адрес, координаты которого вы хотите узнать:').strip()
            if not address:
                continue
            try:
                addresses_list = api.get_location_list_by_address(address)
            except HTTPStatusError:
                print('Проверьте правильность введеных вами данных(api-ключ, секретный ключ)')
                break
            if addresses_list:
                for index, address in enumerate(addresses_list, start=1):
                    print(f'{index}. ' + address['unrestricted_value'])
                print(f'{len(addresses_list) + 1}. Ввести адрес заново')
                print(f'{len(addresses_list) + 2}. Вернуться в меню конфигураций')
                print(f'{len(addresses_list) + 3}. Выход')
                choice = int(input('Выберите один из вариантов:'))
                if choice not in range(1, len(addresses_list) + 2) or choice > len(addresses_list):
                    if choice - 3 == len(addresses_list):
                        return
                    if choice - 2 == len(addresses_list):
                        break
                    continue
                result = str(api.get_location_coordinates_by_address(addresses_list[choice - 1]['unrestricted_value']))
            else:
                result = 'Совпадений не найдено'
            print(result)
            continue


if __name__ == '__main__':
    menu()
